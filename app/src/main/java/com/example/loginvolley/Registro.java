package com.example.loginvolley;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Registro extends AppCompatActivity {
    private TextView Login;
    private Button Registrarse;
    private EditText Correo;
    private EditText Contrasenia;
    private EditText repetirContrasenia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        getSupportActionBar().hide();
        Registrarse =(Button) findViewById(R.id.btnRegistrarme);
        Correo=(EditText) findViewById(R.id.edcorreoRegistro);
        Contrasenia=(EditText) findViewById(R.id.edcontraseniaRegistro);
        repetirContrasenia=(EditText) findViewById(R.id.edrepetirContraseniaRegistro);
        Login=(TextView) findViewById(R.id.txtvcrearcuenta2);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Login = new Intent(Registro.this, Login.class);
                startActivity(Login);
            }
        });

        Registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(Correo.getText().toString()))
                    Correo.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(Contrasenia.getText().toString()))
                    Contrasenia.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(repetirContrasenia.getText().toString()))
                    repetirContrasenia.setError("Este campo no puede estar vacio");
                else if (!TextUtils.equals(Contrasenia.getText(),repetirContrasenia.getText()))
                    repetirContrasenia.setError("La contraseña no coincide");
                else{
                    peticion();

                }
            }
        });
    }
    private void peticion(){
        RequestQueue requestQueue= Volley.newRequestQueue(Registro.this);
        String url = "https://notificacionupt.andocodeando.net/api/crearUsuario";
        Map<String, String> parametros = new HashMap();
        parametros.put("username",Correo.getText().toString());
        parametros.put("password",Contrasenia.getText().toString());
        JSONObject parameters = new JSONObject(parametros);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson=new Gson();
                        JsonParser jsonParser = new JsonParser();
                        JsonObject gsonObject = (JsonObject)jsonParser.parse(response.toString());
                        Registro_Usuario peticionregistro=gson.fromJson(gsonObject,Registro_Usuario.class);
                        if (peticionregistro.estado=="true"){
                            startActivity(new Intent(Registro.this, Login.class));
                            Toast.makeText(Registro.this,"Registro exitoso",Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(Registro.this, peticionregistro.detalle, Toast.LENGTH_SHORT).show();
                        }

                        //textView.setText("Response: " + response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(Registro.this, "error :c", Toast.LENGTH_SHORT).show();

                    }
                });
        requestQueue.add(jsonObjectRequest);

    }
}
