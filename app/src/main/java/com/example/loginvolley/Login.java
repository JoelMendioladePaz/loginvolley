package com.example.loginvolley;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Login extends AppCompatActivity {

    private TextView Crearcuenta;
    private EditText Correo;
    private EditText Contraseña;
    private Button Login;
    private String APITOKEN;
    private String usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        Crearcuenta=(TextView) findViewById(R.id.txtvcrearcuenta);
        Correo=(EditText) findViewById(R.id.edtCorreo);
        Contraseña=(EditText) findViewById(R.id.edtContrasenia);
        Login = (Button) findViewById(R.id.btnIniciarsesion);
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token=preferencias.getString("Token","");
        if (token !=""){
            startActivity(new Intent(Login.this, Menu.class));
            Toast.makeText(this,"Su sesion aun es valida", Toast.LENGTH_SHORT).show();
        }

        Crearcuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Registro = new Intent(Login.this, Registro.class);
                startActivity(Registro);
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(Correo.getText().toString()))
                    Correo.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(Contraseña.getText().toString()))
                    Contraseña.setError("Este campo no puede estar vacio");
                else{
                    peticion();
                    /*if(peticion.estado == "true") {
                        APITOKEN = peticion.token;
                        usuario = peticion.usuario;
                        guardarPreferencias();
                        startActivity(new Intent(Login.this, Menu.class));
                        Toast.makeText(Login.this, "Usted ha iniciado sesion", Toast.LENGTH_SHORT).show();
                    }*/
                }
            }
        });
    }
    private void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= preferences.edit();
        editor.putString("Token",APITOKEN);
        editor.putString("Usuario",usuario);
        editor.commit();
    }
    private void peticion(){
        RequestQueue requestQueue= Volley.newRequestQueue(Login.this);
        String url = "https://notificacionupt.andocodeando.net/api/login";
        Map<String, String> parametros = new HashMap();
        parametros.put("username",Correo.getText().toString());
        parametros.put("password",Contraseña.getText().toString());
        JSONObject parameters = new JSONObject(parametros);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson=new Gson();
                        JsonParser jsonParser = new JsonParser();
                        JsonObject gsonObject = (JsonObject)jsonParser.parse(response.toString());
                        Peticion_Login peticionLogin=gson.fromJson(gsonObject,Peticion_Login.class);
                        if (peticionLogin.estado=="true"){
                            APITOKEN=peticionLogin.token;
                            usuario=peticionLogin.usuario;
                            guardarPreferencias();
                            startActivity(new Intent(Login.this, Menu.class));
                            Toast.makeText(Login.this,"Usted ha iniciado sesion",Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(Login.this, peticionLogin.detalle, Toast.LENGTH_SHORT).show();
                        }

                        //textView.setText("Response: " + response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(Login.this, "error :c", Toast.LENGTH_SHORT).show();

                    }
                });
        requestQueue.add(jsonObjectRequest);

    }
}
